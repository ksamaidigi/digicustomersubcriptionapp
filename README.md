# Customer Subscription Application

This application consists of a REST API backend created in Python/Flask and a frontend created in ReactJS

# Requirements

-   Linux OS
-   Python 3
-   NodeJS
-   Database (e.g. PostgreSQL)
-   NGINX Web Server

# Installation

-   Clone this repository to your Linux server
-   Optionally, create a Python virtual environment in the directory using virtualenv or venv
-   Use pip to install the project requirements: `pip install -r requirements.txt`
-   Use pip to install the python driver for the database you will be using. See https://docs.sqlalchemy.org/en/13/core/engines.html for a list of supported drivers.
-   Copy the config.json.sample file to config.json and populate based on your environment
    -   Set SQLALCHEMY_DATABASE_URI to your DB connection. You may need to install additional packages based on your database. The PostgreSQL package is included in the requirements.txt
    -   Enter a secret key under JWT_SECRET_KEY. You should use a password generator to generate a strong key.
-   Change the `gunicorn.cfg` file to suit your environment needs.
    -   Change the `bind` parameter to use another port if required.
    -   Change the `workers` parameter to add or remove worker threads as required.
-   `cd` to the `react_client` folder.
-   Install the NodeJS packages for react by running `npm install`
-   Build the React frontend application by running `npm run build`
-   Go back to the project root folder.
-   Initialize the DB by running `python init_db.py`
    -   Alternatively, you can populate the DB with dummy data by running `python seed_db_testing.py`
-   Run the flask web application with `gunicorn --config gunicorn.cfg --daemon app:app`
-   Setup nginx as a reverse proxy to the running gunicorn server.
    -   See https://gunicorn.org/#deployment for an example
-   Go to the URL configured in nginx and ensure webpage loads
    -   The dafault user is `admin@localhost` with password `admin123`

# Testing

This project uses `pytest` to test the Flask API, which is included in the python requirements. To test, cd to the project root and run `pytest --ignore=react_client`.
