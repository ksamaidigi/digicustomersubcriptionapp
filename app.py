import simplejson
from datetime import timedelta
from flask import Flask, jsonify, request, abort
from models import db, ma, Customer, Service, User, CustomerSchema, ServiceSchema, UserSchema, LoginSchema
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__, static_url_path='', static_folder='react_client/build')
app.config.from_json('config.json')
jwt = JWTManager(app)

# SQLAlchemy needs app_context to run in
with app.app_context():
    db.init_app(app)
    ma.init_app(app)

    db.create_all()

    @app.route('/api/login', methods=['POST'])
    def login():
        if not request.json:
            return jsonify({'msg': 'Expecting data as JSON'}), 400

        login_schema = LoginSchema()
        validation_result = login_schema.load(request.json)
        if validation_result.errors:
            return jsonify({
                'msg': 'Errors encountered during data validation',
                'errors': validation_result.errors
            }), 400

        # Check if user exists
        user = User.query.filter_by(
            email_address=request.json['email_address']).first()
        if not user:
            return jsonify({
                'msg': 'No such user or password in system'
            }), 401

        # Check if password correct
        if check_password_hash(user.password, request.json['password']):
            access_token = create_access_token(
                identity=user.email_address, expires_delta=timedelta(minutes=30))
            return jsonify(access_token=access_token), 200

        # Password not correct
        return jsonify({'msg': 'No such user or password in system'}), 401

    @app.route('/api/register', methods=['POST'])
    @jwt_required
    def register():
        if not request.json:
            return jsonify({'msg': 'Expecting data as JSON'}), 400

        user_schema = UserSchema()
        validation_result = user_schema.load(request.json)
        if validation_result.errors:
            return jsonify({
                'msg': 'Errors encountered during data validation',
                'errors': validation_result.errors
            }), 400

        # Determine if another user has that email address
        query_user = User.query.filter_by(
            email_address=request.json['email_address']).first()
        if query_user:
            return jsonify({
                'msg': 'Email address is taken'
            }), 400

        # Get the user object and hash the password
        user = validation_result.data
        user.password = generate_password_hash(user.password, method="sha256")
        db.session.add(user)
        db.session.commit()

        # Create a new schema that does not give out the password.
        user_schema = UserSchema(exclude=['password'])
        return jsonify({
            'msg': 'User created successfully',
            'user': user_schema.dump(user).data
        }), 200

    @app.route('/api/customers', methods=['GET'])
    @jwt_required
    def get_all_customers():
        customers = Customer.query.all()
        if not customers:
            return jsonify({'msg': 'No customers in system.'}), 404

        # Generate a Schema for customers. Exclude the customers array in the services object.
        customers_schema = CustomerSchema(
            many=True, exclude=['services.customers'])
        return jsonify(customers_schema.dump(customers).data), 200

    @app.route('/api/customers', methods=['POST'])
    @jwt_required
    def create_customer():
        if not request.json:
            return jsonify({'msg': 'Expecting data as JSON'}), 400

        # Validate user input against schema
        customer_schema = CustomerSchema()
        validation_result = customer_schema.load(request.json)
        if validation_result.errors:
            return jsonify({
                'msg': 'Errors encountered during validation',
                'errors': validation_result.errors
            }), 400

        # If no errors, create a new instance
        customer = validation_result.data
        db.session.add(customer)
        db.session.commit()

        return jsonify({
            'msg': 'Customer created successfully',
            'customer': customer_schema.dump(customer).data
        }), 200

    @app.route('/api/customers/<int:id>', methods=['GET'])
    @jwt_required
    def get_customer(id: int):
        customer = Customer.query.get(id)
        if not customer:
            abort(404)

        customer_schema = CustomerSchema(exclude=['services.customers'])
        return jsonify(customer_schema.dump(customer).data), 200

    @app.route('/api/customers/<int:id>', methods=['PATCH'])
    @jwt_required
    def subscribe_customer_to_services(id: int):
        if not request.json:
            return jsonify({'msg': 'Expecting data as JSON'}), 400

        # Check if customer exists
        customer = Customer.query.get(id)
        if not customer:
            return jsonify({'msg': 'Customer not found'}), 404

        # Check if service ids are provided as list of ints
        service_ids = request.json['services']
        if not (isinstance(service_ids, list) and all(isinstance(i, int) for i in service_ids)):
            return jsonify({'msg': 'Service ID list is invalid'}), 400

        # Get services from the service ids. It's fine if some don't exist. Send 404 if no services exist
        services = Service.query.filter(Service.id.in_(service_ids)).all()

        # Allow empty lists (e.g. if want to remove all subscriptions) by only checking validity if list has data.
        if service_ids and not services:
            return jsonify({'msg': 'No services in list are in system'}), 404

        # Replace old service id list with those returned by query
        customer.services = services
        db.session.commit()

        customer_schema = CustomerSchema(exclude=['services.customers'])
        return jsonify({
            'msg': 'Customer subscriptions updated',
            'customer': customer_schema.dump(customer).data
        }), 200

    @app.route('/api/services', methods=['GET'])
    @jwt_required
    def get_services():
        services = Service.query.all()
        if not services:
            return jsonify({'msg': 'No services in the system'}), 404

        service_schema = ServiceSchema(many=True)
        return jsonify(service_schema.dump(services).data), 200

    # Serve the react client
    @app.route('/', methods=['GET'])
    @app.route('/<string:resource>', methods=['GET'])
    def serve_client(resource=None):
        return app.send_static_file('index.html')


if __name__ == "__main__":
    app.run()
