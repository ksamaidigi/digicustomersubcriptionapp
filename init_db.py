from flask import Flask
from models import db, Customer, Service, User
from werkzeug import generate_password_hash

app = Flask(__name__)
app.config.from_json('config.json')

with app.app_context():
    db.init_app(app)
    db.drop_all()
    db.create_all()

    user = User(
        email_address="admin@localhost",
        first_name="Admin",
        last_name="User",
        password=generate_password_hash("admin123", method="sha256")
    )

    s1 = Service(name="Postpaid 1", price=200.0)
    s2 = Service(name="Postpaid 2", price=300.0)
    s3 = Service(name="Home Internet 1", price=350.0)
    s4 = Service(name="Home Cable TV", price=250.0)

    db.session.add(user)
    db.session.add(s1)
    db.session.add(s2)
    db.session.add(s3)
    db.session.add(s4)
    db.session.commit()

print("Database initialized!")
