from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import fields, Schema

db = SQLAlchemy()
ma = Marshmallow()


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email_address = db.Column(db.String(100), nullable=False, unique=True)
    first_name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(100), nullable=False)


class Customer(db.Model):
    __tablename__ = 'customers'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(100), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    email_address = db.Column(db.String(100), nullable=True)
    contact_number = db.Column(db.String(100), nullable=True)
    services = db.relationship('Service', secondary='subscriptions')


class Service(db.Model):
    __tablename__ = 'services'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    price = db.Column(db.Numeric, nullable=False)
    customers = db.relationship('Customer', secondary='subscriptions')


class Subscription(db.Model):
    __tablename__ = 'subscriptions'
    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    service_id = db.Column(db.Integer, db.ForeignKey('services.id'))


# SCHEMAS

class LoginSchema(Schema):
    email_address = fields.Email(required=True)
    password = fields.String(required=True)


class UserSchema(ma.ModelSchema):
    # Override the email address field to be email
    email_address = fields.Email(required=True)

    class Meta:
        model = User


class ServiceSchema(ma.ModelSchema):
    class Meta:
        model = Service


class CustomerSchema(ma.ModelSchema):
    # Override the email address field to be email
    email_address = fields.Email(required=False)
    services = ma.Nested(ServiceSchema, many=True)

    class Meta:
        model = Customer
