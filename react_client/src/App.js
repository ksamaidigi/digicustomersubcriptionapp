import React from "react";
import "./App.css";
import { Route, BrowserRouter as Router } from "react-router-dom";
import Navbar from "./components/navbar";
import CustomerList from "./components/customerList";
import LoginForm from "./components/loginForm";
import RegistrationForm from "./components/registrationForm";
import NewCustomerForm from "./components/newCustomerForm";

function App() {
    return (
        <Router>
            <div className="App">
                <Navbar />
                <div className="container">
                    <Route exact path="/" component={CustomerList} />
                    <Route path="/customers/new" component={NewCustomerForm} />
                    <Route exact path="/customers" component={CustomerList} />
                    <Route path="/login" component={LoginForm} />
                    <Route path="/register" component={RegistrationForm} />
                </div>
            </div>
        </Router>
    );
}

export default App;
