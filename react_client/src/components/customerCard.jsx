import React, { Component } from "react";
import SubscriptionList from "./subscriptionList";

class CustomerCard extends Component {
    state = {
        customer: this.props.customer
    };

    render() {
        return (
            <div className="col-lg-4 col-sm-12 p-2 small">
                <div className="card h-100">
                    <div className="card-header font-weight-bold">
                        {this.state.customer.last_name},{" "}
                        {this.state.customer.first_name}
                    </div>
                    <div className="card-body">
                        <strong>Email address:</strong>{" "}
                        {this.state.customer.email_address}
                        <br />
                        <strong>Contact number:</strong>{" "}
                        {this.state.customer.contact_number}
                        <SubscriptionList
                            services={this.state.customer.services}
                            customerId={this.state.customer.id}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default CustomerCard;
