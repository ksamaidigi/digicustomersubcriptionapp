import React, { Component } from "react";
import { Link } from "react-router-dom";
import CustomerCard from "./customerCard";
import axios from "axios";

class CustomerList extends Component {
    state = {
        customers: [],
        filtered_customers: [],
        modal_show: false
    };

    constructor(props) {
        super(props);

        if (localStorage.getItem("user_token") === null) {
            props.history.push("/login");
        }
    }

    // Fetch customers from the API
    componentDidMount() {
        axios
            .get("/api/customers", {
                headers: {
                    Authorization:
                        "Bearer " + localStorage.getItem("user_token")
                }
            })
            .then(result => {
                this.setState({ customers: result.data });
                this.setState({ filtered_customers: result.data });
            })
            .catch(error => {
                if (error.response.status === 401) {
                    localStorage.removeItem("user_token");
                    alert(
                        "You have been logged out: " + error.response.data.msg
                    );
                    this.props.history.push("/login");
                }
            });
    }

    filterCustomers = event => {
        let filtered_customers = this.state.customers.filter(
            customer =>
                customer.first_name
                    .toLowerCase()
                    .includes(event.target.value.toLowerCase()) ||
                customer.last_name
                    .toLowerCase()
                    .includes(event.target.value.toLowerCase())
        );
        this.setState({ filtered_customers });
    };

    render() {
        return (
            <div className="row">
                <div className="col">
                    <div className="row">
                        <div className="col-9">
                            <input
                                type="text"
                                className="form-control"
                                id="customer_filter_bar"
                                placeholder="Filter by name..."
                                onChange={this.filterCustomers}
                            />
                        </div>
                        <div className="col-3 text-right">
                            <Link
                                to="/customers/new"
                                className="btn btn-outline-primary"
                            >
                                New Customer
                            </Link>
                        </div>
                    </div>
                    <div className="row">
                        {this.state.filtered_customers.map(customer => (
                            <CustomerCard
                                key={customer.id}
                                customer={customer}
                            />
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

export default CustomerList;
