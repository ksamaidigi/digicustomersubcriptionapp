import React, { Component } from "react";
import axios from "axios";

class LoginForm extends Component {
    state = {
        email: "",
        password: ""
    };

    constructor(props) {
        super(props);

        // If user token present, redirect to secure page
        if (localStorage.getItem("user_token") !== null) {
            props.history.push("/");
        }
    }

    changeStateVars = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    doLogin = event => {
        event.preventDefault();
        axios
            .post("api/login", {
                email_address: this.state.email,
                password: this.state.password
            })
            .then(result => {
                localStorage.setItem("user_token", result.data.access_token);
                this.props.history.push("/");
            })
            .catch(error => {
                if (error.response.status === 401) {
                    alert("No user with this email or password in system");
                    this.props.history.push("/login");
                }
            });
    };

    render() {
        return (
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Agent Login</div>
                        <div className="card-body">
                            <form
                                action=""
                                method="post"
                                onSubmit={this.doLogin}
                            >
                                <div className="form-group row">
                                    <label
                                        htmlFor="email_address"
                                        className="col-sm-4 col-form-label text-md-right"
                                    >
                                        Email Address
                                    </label>
                                    <div className="col-md-6">
                                        <input
                                            id="email"
                                            type="email"
                                            className="form-control"
                                            name="email"
                                            onChange={this.changeStateVars}
                                            required
                                            autoFocus
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label
                                        htmlFor="password"
                                        className="col-sm-4 col-form-label text-md-right"
                                    >
                                        Password
                                    </label>
                                    <div className="col-md-6">
                                        <input
                                            id="password"
                                            type="password"
                                            className="form-control"
                                            name="password"
                                            onChange={this.changeStateVars}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="form-group row mb-0">
                                    <div className="col-md-8 offset-md-4">
                                        <button
                                            type="submit"
                                            className="btn btn-primary"
                                        >
                                            Login
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoginForm;
