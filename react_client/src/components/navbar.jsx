import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
    state = {
        mobile_menu_show: false
    };

    toggleMobileMenu = () => {
        this.setState({ mobile_menu_show: !this.state.mobile_menu_show });
    };

    getMobileMenuClasses = () => {
        let mobile_menu_classes = "collapse navbar-collapse";
        mobile_menu_classes += this.state.mobile_menu_show ? " show" : "";
        return mobile_menu_classes;
    };

    doLogout = () => {
        localStorage.removeItem("user_token");
    };

    render() {
        return (
            <div className="navbar navbar-expand-md navbar-light bg-light mb-4">
                <div className="container">
                    <div className="navbar-brand">
                        Customer Subscription App
                    </div>

                    <button
                        className="navbar-toggler"
                        type="button"
                        onClick={this.toggleMobileMenu}
                    >
                        <span className="navbar-toggler-icon" />
                    </button>

                    <div className={this.getMobileMenuClasses()}>
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link to="/register" className="nav-link">
                                    Register Agent
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/customers" className="nav-link">
                                    Customers
                                </Link>
                            </li>
                        </ul>
                        <ul className="navbar-nav ml-auto text-right">
                            <li className="nav-item">
                                <a
                                    href="/login"
                                    className="btn btn-link nav-link"
                                    onClick={this.doLogout}
                                >
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Navbar;
