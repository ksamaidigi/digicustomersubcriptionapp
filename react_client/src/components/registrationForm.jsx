import React, { Component } from "react";
import axios from "axios";

class RegistrationForm extends Component {
    state = {
        first_name: "",
        last_name: "",
        email_address: "",
        password: "",
        password_confirm: ""
    };

    constructor(props) {
        super(props);

        if (localStorage.getItem("user_token") === null) {
            props.history.push("/login");
        }
    }

    changeStateVars = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    doRegister = event => {
        event.preventDefault();

        if (this.state.password !== this.state.password_confirm) {
            return;
        }

        axios
            .post("/api/register", this.state, {
                headers: {
                    Authorization:
                        "Bearer " + localStorage.getItem("user_token")
                }
            })
            .then(result => {
                alert("Agent created.");
                this.props.history.push("/");
            })
            .catch(error => {
                if (error.response.status === 401) {
                    localStorage.removeItem("user_token");
                    alert(
                        "You have been logged out: " + error.response.data.msg
                    );
                    this.props.history.push("/login");
                } else if (
                    error.response.status === 400 &&
                    error.response.data.msg.includes("validation")
                ) {
                    // Display validation errors
                    let error_list = "";
                    const errors = error.response.data.errors;
                    for (var key in errors) {
                        error_list += "\n - " + key + " - " + errors[key];
                    }

                    alert("There were validation errors: " + error_list);
                } else {
                    alert("There was an error: " + error.response.data.msg);
                }
            });
    };

    render() {
        return (
            <div className="row justify-content-center">
                <div className="col-lg-8">
                    <div className="card">
                        <div className="card-header">Register New Agent</div>
                        <div className="card-body">
                            <form
                                action="#"
                                method="post"
                                onSubmit={this.doRegister}
                            >
                                <div className="form-group row">
                                    <label
                                        htmlFor="first_name"
                                        className="col-md-4 col-form-label text-md-right"
                                    >
                                        Name
                                    </label>
                                    <div className="col-md-6">
                                        <div className="input-group">
                                            <input
                                                type="text"
                                                name="first_name"
                                                id="first_name"
                                                placeholder="First"
                                                className="form-control"
                                                onChange={this.changeStateVars}
                                            />
                                            <input
                                                type="text"
                                                name="last_name"
                                                id="last_name"
                                                placeholder="Last"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label
                                        htmlFor="email_address"
                                        className="col-md-4 col-form-label text-md-right"
                                    >
                                        Email Address
                                    </label>
                                    <div className="col-md-6">
                                        <input
                                            type="email"
                                            name="email_address"
                                            id="email_address"
                                            className="form-control"
                                            onChange={this.changeStateVars}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label
                                        htmlFor="password"
                                        className="col-md-4 col-form-label text-md-right"
                                    >
                                        Password
                                    </label>
                                    <div className="col-md-6">
                                        <input
                                            type="password"
                                            name="password"
                                            id="password"
                                            className="form-control"
                                            onChange={this.changeStateVars}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label
                                        htmlFor="password_confirm"
                                        className="col-md-4 col-form-label text-md-right"
                                    >
                                        Confirm Password
                                    </label>
                                    <div className="col-md-6">
                                        <input
                                            type="password"
                                            name="password_confirm"
                                            id="password_confirm"
                                            className="form-control"
                                            onChange={this.changeStateVars}
                                        />
                                    </div>
                                </div>

                                <div className="form-group row mb-0">
                                    <div className="col-md-6 offset-md-4">
                                        <button
                                            type="submit"
                                            className="btn btn-primary"
                                        >
                                            Register
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default RegistrationForm;
