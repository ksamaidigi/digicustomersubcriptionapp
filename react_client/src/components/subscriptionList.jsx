import React, { Component } from "react";
import axios from "axios";

class SubscriptionList extends Component {
    state = {
        edit_mode: false,
        services: this.props.services,
        all_services: [],
        edit_services_selected: []
    };

    handleBeginEditSubscriptions = () => {
        axios
            .get("/api/services", {
                headers: {
                    Authorization:
                        "Bearer " + localStorage.getItem("user_token")
                }
            })
            .then(result => {
                // Add a selected item in array so user knows which he has
                this.setState({
                    all_services: result.data.map(item => {
                        item.selected = this.state.services.some(
                            local_item => local_item.id === item.id
                        );
                        return item;
                    })
                });

                // Use the above array to create one with only the selected IDs
                this.setState({
                    edit_services_selected: this.state.all_services.filter(
                        item => item.selected
                    )
                });
                this.setState({
                    edit_services_selected: this.state.edit_services_selected.map(
                        item => item.id
                    )
                });

                this.setState({ edit_mode: true });
            })
            .catch(error => {
                if (error.response.status === 401) {
                    localStorage.removeItem("user_token");
                    alert(
                        "You have been logged out: " + error.response.data.msg
                    );
                    this.props.history.push("/login");
                }
            });
    };

    handleEditCancel = () => {
        this.setState({ edit_mode: false });
    };

    handleCheckboxClicked = event => {
        // Check event target for if checkbox checked or not, and pop/push it into the array as such
        const service_id = parseInt(event.target.value);

        if (event.target.checked) {
            //Checkbox was selected, add to list
            let edit_services_selected = [
                ...this.state.edit_services_selected,
                service_id
            ];
            this.setState({ edit_services_selected });
        } else {
            //Checkbox deselected, find index of element and remove from list
            let edit_services_selected = [...this.state.edit_services_selected];
            edit_services_selected.splice(
                edit_services_selected.indexOf(service_id),
                1
            );
            this.setState({ edit_services_selected });
        }
    };

    handleUpdateSubscriptions = () => {
        // Make the post request to insert the data in the DB
        const request_data = {
            services: [...this.state.edit_services_selected]
        };

        axios
            .patch("/api/customers/" + this.props.customerId, request_data, {
                headers: {
                    Authorization:
                        "Bearer " + localStorage.getItem("user_token")
                }
            })
            .then(result => {
                const customer = result.data.customer;
                this.setState({ services: customer.services });
                this.setState({ edit_mode: false });
            })
            .catch(error => {
                if (error.response.status === 401) {
                    localStorage.removeItem("user_token");
                    alert(
                        "You have been logged out: " + error.response.data.msg
                    );
                    this.props.history.push("/login");
                } else if (
                    error.response.status === 400 &&
                    error.response.data.msg.includes("validation")
                ) {
                    // Display validation errors
                    let error_list = "";
                    const errors = error.response.data.errors;
                    for (var key in errors) {
                        error_list += "\n - " + key + " - " + errors[key];
                    }

                    alert("There were validation errors: " + error_list);
                } else {
                    alert("There was an error: " + error.response.data.msg);
                }
            });

        this.setState({ edit_mode: false });
    };

    render() {
        if (this.state.edit_mode) {
            return (
                <div>
                    <strong>Subscriptions:</strong>{" "}
                    <form
                        action="#"
                        method="post"
                        onSubmit={this.handleUpdateSubscriptions}
                    >
                        <button
                            className="btn btn-link btn-sm align-baseline"
                            onClick={this.handleEditCancel}
                        >
                            Cancel
                        </button>
                        <button
                            className="btn btn-link btn-sm align-baseline"
                            type="submit"
                        >
                            Done
                        </button>

                        {this.state.all_services.map(service => (
                            <div className="form-check" key={service.id}>
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    key={service.id}
                                    name="services[]"
                                    value={service.id}
                                    checked={this.state.edit_services_selected.includes(
                                        service.id
                                    )}
                                    onChange={this.handleCheckboxClicked}
                                    id={"services_" + service.id}
                                />
                                <label
                                    htmlFor={"services_" + service.id}
                                    className="form-check-label"
                                />
                                {service.name} - ${service.price.toFixed(2)}
                            </div>
                        ))}
                    </form>
                </div>
            );
        } else {
            // Not in edit mode
            return (
                <div>
                    <strong>Subscriptions:</strong>{" "}
                    <button
                        className="btn btn-link btn-sm align-baseline"
                        onClick={this.handleBeginEditSubscriptions}
                    >
                        Edit
                    </button>
                    <ul>
                        {this.state.services.map(service => (
                            <li key={service.id}>{service.name}</li>
                        ))}
                    </ul>
                </div>
            );
        }
    }
}

export default SubscriptionList;
