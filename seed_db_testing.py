from flask import Flask
from models import db, Customer, Service, User
from werkzeug import generate_password_hash

app = Flask(__name__)
app.config.from_json('config.json')

with app.app_context():
    db.init_app(app)
    db.drop_all()
    db.create_all()

    user = User(
        email_address="admin@localhost",
        first_name="Admin",
        last_name="User",
        password=generate_password_hash("admin123", method="sha256")
    )

    # Follow the convention when adding data to be seeded.
    c1 = Customer(first_name="Kenneth", last_name="Samai",
                  email_address="ken@digi.com", contact_number="12345")
    c2 = Customer(first_name="Charlie", last_name="Brown",
                  email_address="snoopy@digi.com", contact_number="12345")
    c3 = Customer(first_name="Rex", last_name="Dangervest",
                  email_address="lego@digi.com", contact_number="12345")
    c4 = Customer(first_name="Iroquois", last_name="Pliskin",
                  email_address="snake@digi.com", contact_number="12345")

    s1 = Service(name="Postpaid 1", price=200.0)
    s2 = Service(name="Postpaid 2", price=300.0)
    s3 = Service(name="Home Internet 1", price=350.50)
    s4 = Service(name="Home Cable TV", price=250.0)

    db.session.add(user)

    db.session.add(c1)
    db.session.add(c2)
    db.session.add(c3)
    db.session.add(c4)

    db.session.add(s1)
    db.session.add(s2)
    db.session.add(s3)
    db.session.add(s4)

    # Setup any initial services for the customers
    c1.services.append(s2)
    c1.services.append(s4)
    c3.services.append(s1)
    c4.services.append(s3)

    # You can also just replace the services array
    c3.services = [s2, s4]

    # Commit the session
    db.session.commit()

print("Database seeded!")
