import os
import tempfile
import pytest
import simplejson
from app import app
from models import db, ma, Customer, Service, User, CustomerSchema, ServiceSchema, UserSchema, LoginSchema
from werkzeug.security import generate_password_hash


@pytest.fixture
def client():
    # Make a tempfile for a SQLite DB to use instead of live DB
    db_fd, filename = tempfile.mkstemp()
    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///{}".format(filename)
    app.config['TESTING'] = True
    client = app.test_client()

    with app.app_context():
        db.init_app(app)
        ma.init_app(app)
        db.create_all()

        # Add a user to test with and some services to play with
        user = User(
            email_address="admin@localhost",
            first_name="Admin",
            last_name="User",
            password=generate_password_hash("admin123", method="sha256")
        )
        s1 = Service(name="Postpaid 1", price=200.0)
        s2 = Service(name="Postpaid 2", price=300.0)
        s3 = Service(name="Home Internet 1", price=350.0)
        s4 = Service(name="Home Cable TV", price=250.0)

        db.session.add(user)
        db.session.add(s1)
        db.session.add(s2)
        db.session.add(s3)
        db.session.add(s4)
        db.session.commit()

    yield client

    os.close(db_fd)


## HELPER FUNCTIONS ##


def login(client, email_address: str, password: str):
    return client.post("/api/login",
                       data=simplejson.dumps(dict(email_address=email_address,
                                                  password=password)),
                       content_type="application/json",
                       follow_redirects=True)


def register(client, agent: list, access_token: str):
    return client.post('/api/register', data=simplejson.dumps(agent),
                       headers={"Authorization": "Bearer {}".format(access_token),
                                "Content-Type": "application/json"})


def get_customers(client, access_token: str):
    return client.get(
        '/api/customers', headers={"Authorization": "Bearer {}".format(access_token)})


def get_single_customer(client, customer_id: int, access_token: str):
    return client.get("/api/customers/{}".format(customer_id),
                      headers={"Authorization": "Bearer {}".format(access_token)})


def create_new_customer(client, customer: list, access_token: str):
    return client.post('/api/customers', data=simplejson.dumps(customer),
                       headers={"Authorization": "Bearer {}".format(access_token),
                                "Content-Type": "application/json"})


def create_subscription(client, customer_id: int, service_ids: list, access_token: str):
    return client.patch("/api/customers/{}".format(customer_id),
                        data=simplejson.dumps({"services": service_ids}),
                        headers={"Authorization": "Bearer {}".format(access_token),
                                 "Content-Type": "application/json"})

## ACTUAL TESTS ##


def test_login(client):
    # Test correct login
    email_address = "admin@localhost"
    password = "admin123"
    result = login(client, email_address, password)
    assert result.status_code == 200
    data = result.get_json()
    assert data['access_token']

    # Test invalid login
    email_address = "testing@hello.com"
    result = login(client, email_address, password)
    data = result.get_json()
    assert result.status_code == 401 and "No such user or password in system" in data['msg']

    # Test send bad email
    email_address = "testing"
    result = login(client, email_address, password)
    data = result.get_json()
    assert result.status_code == 400 and "Errors encountered during data validation" in data[
        'msg']

    # Test non-JSON request
    result = client.post("/api/login",
                         data=dict(email_address=email_address,
                                   password=password),
                         follow_redirects=True)
    data = result.get_json()
    assert result.status_code == 400 and "Expecting data as JSON" in data['msg']


def test_registration(client):
    # Login
    result = login(client, "admin@localhost", "admin123")
    access_token = result.get_json()['access_token']

    # Test creating a user
    agent = {"first_name": "Test", "last_name": "Agent",
             "email_address": "agent@localhost", "password": "agent123"}
    result = register(client, agent, access_token)
    assert result.status_code == 200

    # Test login with new user
    result = login(client, agent["email_address"], agent["password"])
    data = result.get_json()
    assert result.status_code == 200
    assert data['access_token']

    # Test creating a user with bad data
    agent["first_name"] = ["Hello"]
    result = register(client, agent, access_token)
    data = result.get_json()
    assert result.status_code == 400 and "validation" in data['msg']

    agent["email_address"] = "wrong format :("
    result = register(client, agent, access_token)
    data = result.get_json()
    assert result.status_code == 400 and "validation" in data['msg']

    # Test creating user without token
    result = client.post('/api/register', data=simplejson.dumps(agent),
                         headers={"Content-Type": "application/json"})
    assert result.status_code == 401


def test_customers(client):
    # Login
    result = login(client, "admin@localhost", "admin123")
    access_token = result.get_json()['access_token']

    # Get customers from empty DB
    result = get_customers(client, access_token)
    assert result.status_code == 404

    # Add a new customer
    new_customer = {"first_name": "Test", "last_name": "User",
                    "email_address": "test@example.com", "contact_number": "12345678"}
    result = create_new_customer(client, new_customer, access_token)
    data = result.get_json()
    assert result.status_code == 200 and "Customer created successfully" in data['msg']
    customer_id = data['customer']['id']

    # Get newly created customer from DB by getting all customers
    result = get_customers(client, access_token)
    assert result.status_code == 200
    data = result.get_json()
    # Ensure an array is returned, has only one element and matches the customer we created
    assert isinstance(data, list) and len(data) == 1
    assert data[0]['first_name'] == new_customer['first_name']
    assert data[0]['last_name'] == new_customer['last_name']
    assert data[0]['email_address'] == new_customer['email_address']
    assert data[0]['contact_number'] == new_customer['contact_number']

    # Get new customer by querying by ID
    result = get_single_customer(client, customer_id, access_token)
    assert result.status_code == 200
    data = result.get_json()
    assert data['first_name'] == new_customer['first_name']
    assert data['last_name'] == new_customer['last_name']
    assert data['email_address'] == new_customer['email_address']
    assert data['contact_number'] == new_customer['contact_number']

    # Test creating a user with bad data
    new_customer["first_name"] = ["Hello"]
    result = create_new_customer(client, new_customer, access_token)
    data = result.get_json()
    assert result.status_code == 400 and "validation" in data['msg']

    new_customer["email_address"] = "wrong format :("
    result = create_new_customer(client, new_customer, access_token)
    data = result.get_json()
    assert result.status_code == 400 and "validation" in data['msg']

    # Get customers without token:
    result = client.get('/api/customers')
    assert result.status_code == 401

    # Get customers with bad token
    result = client.get(
        '/api/customers', headers={"Authorization": "Bearer {}".format("not_a_token")})
    assert result.status_code == 422

    # Create customer with no token
    result = client.post('/api/customers', data=simplejson.dumps(new_customer),
                         headers={"Content-Type": "application/json"})
    assert result.status_code == 401


def test_subscriptions(client):
    # Login
    result = login(client, "admin@localhost", "admin123")
    access_token = result.get_json()['access_token']

    # Create a user
    new_customer = {"first_name": "Test", "last_name": "User",
                    "email_address": "test@example.com", "contact_number": "12345678"}
    result = create_new_customer(client, new_customer, access_token)
    data = result.get_json()
    customer_id = data['customer']['id']

    # Create some subscriptions for user using hardcoded IDs
    service_ids = [1, 2]
    result = create_subscription(
        client, customer_id, service_ids, access_token)
    assert result.status_code == 200
    data = result.get_json()

    # Get customer an ensure he has above services
    result = get_single_customer(client, customer_id, access_token)
    data = result.get_json()
    customer_service_ids = list(map(
        lambda item: item['id'], data['services']))
    assert customer_service_ids == service_ids

    # Create a user with bad data
    service_ids = ["hello", "world"]
    result = create_subscription(
        client, customer_id, service_ids, access_token)
    assert result.status_code == 400
